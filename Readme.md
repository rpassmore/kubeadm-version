# Kubeadm Version

A container that simply displays the containers and their versions needed by the kubeadm tool to set up a cluster.

The entry point just runs:
```
kubeadm config images list
```


# Example usage
command:
```
docker run rpassmore/kubeadm-version:v1.20.1 
```

output:
```
k8s.gcr.io/kube-apiserver:v1.20.1
k8s.gcr.io/kube-controller-manager:v1.20.1
k8s.gcr.io/kube-scheduler:v1.20.1
k8s.gcr.io/kube-proxy:v1.20.1
k8s.gcr.io/pause:3.2
k8s.gcr.io/etcd:3.4.13-0
k8s.gcr.io/coredns:1.7.0
```

# Usecase
A convinience container to avoid having to install kubeadm and its dependencies just to find out what containers and versions are needed when exporting containers to create a cluster in an airgapped environment.

This list of containers can the be pulled on an internet facing machine, exported to an archive and imported into the airgapped container registry.

A script is provided that will pull the container images and save them to an archive named `k8s-images-<VERSION>.tgz`
```
./getimages.sh
```
These images can then be imported on the airgapped machine using
```
docker load --input k8s-images-v1.20.1.tgz
```


# Building
```
VERSION=v1.20.1

docker build ./ --build-arg RELEASE=${VERSION} -t rpassmore/kubeadm-version:${VERSION}
```