FROM alpine:3.12.1
#RELEASE="$(curl -sSL https://dl.k8s.io/release/stable.txt)"
ARG RELEASE=v1.20.1
RUN wget -q https://storage.googleapis.com/kubernetes-release/release/${RELEASE}/bin/linux/amd64/kubeadm -O /usr/bin/kubeadm
RUN chmod +x /usr/bin/kubeadm

RUN adduser -S kubeuser
USER kubeuser

CMD [ "kubeadm", "config", "images", "list" ]