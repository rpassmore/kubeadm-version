#!/bin/bash
set -e

VERSION="v1.20.1"
OUTPUT="k8s-images-${VERSION}.tgz"

IMAGES=$(docker run rpassmore/kubeadm-version:${VERSION})
for IMAGE in ${IMAGES}
do
    docker pull ${IMAGE}    
done

echo "+ Saving to " ${OUTPUT}
docker save ${IMAGES} -o ${OUTPUT}
